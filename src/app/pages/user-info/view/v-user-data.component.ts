import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';
import { concatMap, map, mergeMap, switchMap } from 'rxjs/operators';

import { HttpRequestsService } from '@services/http-requests/http-requests.service';
import { PostRequestService } from '@services/post-request/post-request.service';
import { UserDataService } from '@services/user-data/user-data.service';
import { AlertPopupComponent } from '@shared/components/popups/alert-popup/alert-popup.component';

@Component({
  selector: 'v-user-data',
  templateUrl: './v-user-data.component.html',
})
export class UserDataComponent implements OnInit {
  public title = 'Delivery table';

  public loginRequest = {
    email: 'nikita.sufranovich@gmail.com',
    password: 'detroit313',
  };
  public loginUrl = 'https://lgt-billing.disposed.xyz:/api/login';
  public allLeadsUrl = 'https://lgt-billing.disposed.xyz:/api/lead_list?skip=0&limit=50';
  public botAnalyticsUrl = 'https://lgt-billing.disposed.xyz:/api/bots/analytics';
  public profileUrl = 'https://lgt-billing.disposed.xyz:/api/user/profile';
  // tslint:disable-next-line:max-line-length
  public accessToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1ODI1NDY4NTksIm5iZiI6MTU4MjU0Njg1OSwianRpIjoiZDY4MmRkZmMtN2M0Yy00YjY4LTgwMjctMTgxNmY5MGE0M2IyIiwiZXhwIjoxNTgzODQyODU5LCJpZGVudGl0eSI6Im5pa2l0YS5zdWZyYW5vdmljaEBnbWFpbC5jb20iLCJmcmVzaCI6ZmFsc2UsInR5cGUiOiJhY2Nlc3MifQ.82pVDqEgMdIEqQ9aj8Ml7sYHHrdWTSb8eJuLIIB7tzM';

  public dataSource = this.objectToArray();
  public displayedColumns = ['accountType', 'firstName', 'lastName', 'country', 'streetAddress', 'streetAddress2', 'city', 'state', 'zip', 'phone'];
  public httpError: any;

  constructor(
    public readonly userDataService: UserDataService,
    public readonly postRequestService: PostRequestService,
    public readonly httpRequestsService: HttpRequestsService,
    public readonly matDialog: MatDialog,
  ) { }

  public ngOnInit(): void {
    /*switchMap*/
    console.log('switchMap');
    this.httpRequestsService.postLoginRequest$(this.loginUrl, this.loginRequest)
      .pipe(
        switchMap(
          data => this.httpRequestsService.getAllLeadsRequest$(this.allLeadsUrl, data.access_token),
        ),
      )
      .subscribe((response) => {
          console.log(response);
        },
      );

    /*forkJoin*/
    /*console.log('forkJoin');
    forkJoin([
      this.httpRequestsService.getAllLeadsRequest$(this.allLeadsUrl, this.accessToken),
      this.httpRequestsService.getBotAnalyticsRequest$(this.botAnalyticsUrl, this.accessToken),
      this.httpRequestsService.getProfileRequest$(this.profileUrl, this.accessToken),
    ])
    .subscribe((response) => {
      console.log(response);
    });*/

    /*concatMap*/
    /* console.log('concatMap');
    this.httpRequestsService.getAllLeadsRequest$(this.allLeadsUrl, this.accessToken)
      .pipe(
        concatMap(user => {
            const bots = this.httpRequestsService.getBotAnalyticsRequest$(this.botAnalyticsUrl, this.accessToken);
            const profile = this.httpRequestsService.getProfileRequest$(this.profileUrl, this.accessToken);

            return forkJoin([bots, profile]);
          },
        ),
      )
      .subscribe((response => {
        console.log(response);
      }));*/

    /*mergeMap*/
    /*console.log('mergeMap');
    this.httpRequestsService.getAllLeadsRequest$(this.allLeadsUrl, this.accessToken)
      .pipe(
        mergeMap( user => {
            const bots = this.httpRequestsService.getBotAnalyticsRequest$(this.botAnalyticsUrl, this.accessToken);
            const profile = this.httpRequestsService.getProfileRequest$(this.profileUrl, this.accessToken);
            const bots1 = this.httpRequestsService.getBotAnalyticsRequest$(this.botAnalyticsUrl, this.accessToken);
            const profile1 = this.httpRequestsService.getProfileRequest$(this.profileUrl, this.accessToken);
            const bots2 = this.httpRequestsService.getBotAnalyticsRequest$(this.botAnalyticsUrl, this.accessToken);
            const profile2 = this.httpRequestsService.getProfileRequest$(this.profileUrl, this.accessToken);

            return forkJoin([bots, profile, bots1, profile1, bots2, profile2]);
          },
        ),
      )
      .subscribe(
        ((response) => {
          console.log(response);
        }),
      );*/

    this.postRequest();
  }

// todo что возвращает метод
  public objectToArray(): string[] {
    const array = [];
    array.push(this.userDataService.userData);

    return array;
  }

  public postRequest(): void {
    this.postRequestService.postDataResponse$(this.loginUrl, this.userDataService.userData)
      .subscribe(data => {
          const response = data;
        },
        error => {
          this.httpError = error;
          this.postRequestError();
        });
  }

  public postRequestError(): void {
    const alertDialog = this.matDialog.open(AlertPopupComponent, {
      width: '550px',
      data: this.httpError,
    });
  }
}
