import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
    selector: 't-checkout',
    templateUrl: './t-user-info.component.html',
    styleUrls: ['./t-user-info.component.scss'],
  })
  export class TableCheckoutComponent {
  @Input() public title: string;
  @Input() public displayedColumns: string[];
  @Input() public dataSource: any;

  @Output() public alertDialog: EventEmitter<void> = new EventEmitter();
  @Output() public postDataRequest: EventEmitter<void> = new EventEmitter();
  // todo: тоже вкрал де-то и не переписал под ангуляр с линтом и стилями + зачем этот метод
}

