import { TableCheckoutComponent } from './table/t-user-info/t-user-info.component';
import { UserDataComponent } from './view/v-user-data.component';

export const USERS_COMPONENTS = [
    TableCheckoutComponent,
    UserDataComponent,
];
