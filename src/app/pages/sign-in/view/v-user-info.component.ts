import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserDataService } from '@services/user-data/user-data.service';
import { urlLinks } from '@shared/constants/urlLinks';

@Component({
  selector: 'v-signIn',
  templateUrl: './v-sign-in.component.html',
})
export class ViewSignInComponent {
  public pageLink = urlLinks;

  constructor(
    public readonly userDataService: UserDataService,
    public readonly router: Router,
  ) {}
}

