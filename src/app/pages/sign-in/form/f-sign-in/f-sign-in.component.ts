import { Component, EventEmitter, Input, Output } from '@angular/core';

import { UserData } from '@shared/models/users.model';

@Component({
  selector: 'f-sign-in',
  templateUrl: './f-sign-in.component.html',
  styleUrls: ['./f-sign-in.component.scss'],
})
export class SignInComponent {
  @Input() public userData: UserData;

  @Output() public addingUser: EventEmitter<void> = new EventEmitter();
}

