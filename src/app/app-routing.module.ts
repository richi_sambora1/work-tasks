import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ViewSignInComponent } from '@pages/sign-in/view/v-user-info.component';
import { UserDataComponent } from '@pages/user-info/view/v-user-data.component';
// todo: work with tslint ALL FILE

const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full' },
  { path: 'sign-in', component: ViewSignInComponent }, // todo: названия фигня, почему компонент называется user info а путь signIn,
  // я бы назвал signIn или createUser
  { path: 'user-data', component: UserDataComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
