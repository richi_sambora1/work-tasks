import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SIGN_COMPONENTS } from '@pages/sign-in';
import { USERS_COMPONENTS } from '@pages/user-info';
import { HeaderComponent } from '@shared/components/header/header.component';
import { AlertPopupComponent } from '@shared/components/popups/alert-popup/alert-popup.component';
import { Services } from './services';

@NgModule({
  declarations: [
    AppComponent,
    SIGN_COMPONENTS,
    USERS_COMPONENTS,
    HeaderComponent,
    AlertPopupComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatDialogModule,
    MatButtonModule,
  ],
  exports: [
    [RouterModule],
  ],
  entryComponents: [
    AlertPopupComponent,
  ],
  providers: [
    Services,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
