import { Component } from '@angular/core';

import { urlLinks } from '@shared/constants/urlLinks';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  public pageLink = urlLinks;
}
