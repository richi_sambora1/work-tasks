import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA,  MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'alert-popup',
  templateUrl: './alert-popup.component.html',
  styleUrls: ['./alert-popup.component.scss'],
})
export class AlertPopupComponent {
  constructor(
    public readonly dialog: MatDialogRef<AlertPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public message: string,
  ) { }

  public closeClick(): void {
    this.dialog.close();
  }
}
