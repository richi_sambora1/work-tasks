 export interface BotsResponse  {
   date: null;
   messages_received: number;
   messages_filtered: number;
 }
 export interface LeadResponse  {
   id: string;
   text: string;
   notes: string;
   status: string;
   created_at: string;
   url: string;
   source: string;
}
 export interface UsersResponse {
  email: string;
  roles: string[];
  user_name: string;
  company: string;
  position: string;
}
 export interface LoginResponse {
   access_token: string;
   email: string;
   roles: string[];
   user_name: string;
   company: string;
   position: string;
 }
