import { HttpRequestsService } from '@services/http-requests/http-requests.service';
import { PostRequestService } from '@services/post-request/post-request.service';
import { UserDataService } from '@services/user-data/user-data.service';

export const Services = [
    HttpRequestsService,
    UserDataService,
    PostRequestService,
];
