import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BotsResponse, LeadResponse, LoginResponse, UsersResponse } from '@shared/models/interfaces.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class HttpRequestsService {
  constructor(
    private readonly http: HttpClient,
  ) { }

  public postLoginRequest$(loginUrl: string, loginRequest: object): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(loginUrl, loginRequest)
      .pipe(
        map(
        (data) => {
          console.log( '0: postLogin');

          return data;
        },
      ));
  }

  public getAllLeadsRequest$(allLeadsUrl: string, accessToken: string): Observable<LeadResponse> {
    return this.http.get<LeadResponse>(allLeadsUrl, {
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    }).pipe(
      (data) => {
      console.log('1: getLeads');

      return data;
    });
  }

  public getBotAnalyticsRequest$(botAnalytics: string, accessToken: string): Observable<BotsResponse> {
    return this.http.get<BotsResponse>(botAnalytics, {
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    }).pipe(
      (data) => {
      console.log('2: getBots');

      return data;
    });
  }

  public getProfileRequest$(leadStatuses: string, accessToken: string): Observable<UsersResponse> {
    return this.http.get<UsersResponse>(leadStatuses, {
      headers: {
        Authorization: 'Bearer ' + accessToken,
      },
    }).pipe(
      (data) => {
      console.log('3: getProfile');

      return data;
    });
  }
}
