import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class PostRequestService {
  constructor(
    private readonly http: HttpClient,
  ) { }

  public postDataResponse$(url: string, dataResponse: object): Observable<any> {
    return this.http
      .post(url, dataResponse)
      .pipe(catchError(this.errorHandler));
  }

  public errorHandler(error: HttpErrorResponse): Observable<any> {
    return throwError(error.message);
  }
}

