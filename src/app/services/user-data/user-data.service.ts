import { Injectable } from '@angular/core';

import { UserData } from '@shared/models/users.model';

@Injectable({
  providedIn: 'root',
})

export class UserDataService {
  public userData: UserData = new UserData();
}
